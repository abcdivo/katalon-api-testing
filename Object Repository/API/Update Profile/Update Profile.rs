<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Profile</name>
   <tag></tag>
   <elementGuidId>65740915-68eb-400f-9d92-f4e488ad439e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZDMyYTM4MTg4ZGY5YTZlMzA3MDNjZWU2NjhhNzEzYjM3Y2ViMTQxZTIzNmViMDJiZTdlNTUxNDFjZTc3YWQyY2YwNzMyMmU5OTFjOWMwOTYiLCJpYXQiOjE2OTgyNjM4MzUuMDgyNzMzLCJuYmYiOjE2OTgyNjM4MzUuMDgyNzM2LCJleHAiOjE3Mjk4ODYyMzUuMDc5MzI1LCJzdWIiOiIxMTMxIiwic2NvcGVzIjpbXX0.YLByl3ZAb7ZuNNesSR6pK6xGZnhns_2xDpd7ysZs1AWbdp7PPztooeXNT20BjKrNJsIexgkNzffmGor_DjMnXoepG7jHwXMpe6RHf7o4VbZ70B9lFRuGQTjxsUWXAnjpfQkDYgrDikt47yiA1DYE92YNAwVo_J3ZmyAjJ17c9fT-4Bo2M6dYs-mQi-gns9NiN3rhYtMDRe5l6XuZ7OE1G5Efxo5h8mwc2CCsRrdGwC6VOntl9dBgQ5FIbkzy-2GlRexj0UFPSTmDPP_YlsymyLnbNdngCA_DqVaJZCNX_TBTq09BaRHgUDsXmN4Z49s24skczPJFLH5OaR5U45NQ6JHcTOOnU8gFeWxTBn5gGVBEkmcgrGDFiQvFhibzb0vQfBkNfiiaxjAsevJ1o79SU7ZhSc33I11ZpbUGmaF4P5-15Bw95Wgg5amrMCsPk7MiaX5S3AX-yxHKmq1ke9g3RE9ccK1qorwidi7P94-6Sh0bJA-EvVBel9wjrOvUxBP_F43ZscftcDYq2M6RyWGBShc998dJw1m_zIY1TsuZKKC2ZzH6fm2jwcjOkDFCSevTaygO7dtd7hNZ86O35A2hpXUUnhRYV8A8BGP9XNNE6PLdIjDuVc3Ag3HlR7qC7af2lggziTEOs_7D0sXmijCXRFhGoFl7y9B0wWD73mK7IM8</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;testing&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;082154657732&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;1995-05-04&quot;
    },
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;Software Dev&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;mobile dev&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
      <webElementGuid>8b8f2673-db5c-4ae0-b388-2636bf2ced5f</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZDMyYTM4MTg4ZGY5YTZlMzA3MDNjZWU2NjhhNzEzYjM3Y2ViMTQxZTIzNmViMDJiZTdlNTUxNDFjZTc3YWQyY2YwNzMyMmU5OTFjOWMwOTYiLCJpYXQiOjE2OTgyNjM4MzUuMDgyNzMzLCJuYmYiOjE2OTgyNjM4MzUuMDgyNzM2LCJleHAiOjE3Mjk4ODYyMzUuMDc5MzI1LCJzdWIiOiIxMTMxIiwic2NvcGVzIjpbXX0.YLByl3ZAb7ZuNNesSR6pK6xGZnhns_2xDpd7ysZs1AWbdp7PPztooeXNT20BjKrNJsIexgkNzffmGor_DjMnXoepG7jHwXMpe6RHf7o4VbZ70B9lFRuGQTjxsUWXAnjpfQkDYgrDikt47yiA1DYE92YNAwVo_J3ZmyAjJ17c9fT-4Bo2M6dYs-mQi-gns9NiN3rhYtMDRe5l6XuZ7OE1G5Efxo5h8mwc2CCsRrdGwC6VOntl9dBgQ5FIbkzy-2GlRexj0UFPSTmDPP_YlsymyLnbNdngCA_DqVaJZCNX_TBTq09BaRHgUDsXmN4Z49s24skczPJFLH5OaR5U45NQ6JHcTOOnU8gFeWxTBn5gGVBEkmcgrGDFiQvFhibzb0vQfBkNfiiaxjAsevJ1o79SU7ZhSc33I11ZpbUGmaF4P5-15Bw95Wgg5amrMCsPk7MiaX5S3AX-yxHKmq1ke9g3RE9ccK1qorwidi7P94-6Sh0bJA-EvVBel9wjrOvUxBP_F43ZscftcDYq2M6RyWGBShc998dJw1m_zIY1TsuZKKC2ZzH6fm2jwcjOkDFCSevTaygO7dtd7hNZ86O35A2hpXUUnhRYV8A8BGP9XNNE6PLdIjDuVc3Ag3HlR7qC7af2lggziTEOs_7D0sXmijCXRFhGoFl7y9B0wWD73mK7IM8</value>
      <webElementGuid>6b5fc416-0a2e-40fc-b544-b9ea37e8537f</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.online/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
